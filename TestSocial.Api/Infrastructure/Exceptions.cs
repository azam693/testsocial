﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSocial.Api.Infrastructure
{
    public class SocialException : Exception
    {
        public SocialException(string message)
            : base(message)
        {

        }
    }
}
