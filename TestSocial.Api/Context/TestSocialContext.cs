﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestSocial.Api.Models;

namespace TestSocial.Api.Context
{
    public class TestSocialContext : DbContext
    {
        public TestSocialContext()
        {

        }

        public DbSet<Person> Person { get; set; }

        public DbSet<Relatives> Relatives { get; set; }
    }
}
