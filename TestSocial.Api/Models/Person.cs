﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestSocial.Api.Infrastructure;

namespace TestSocial.Api.Models
{
    public class Person : EntityBase, ICondition
    {
        [Required]
        [MaxLength(40)]
        public string Name { get; set; }

        [Required]
        [MaxLength(40)]
        public string Surname { get; set; }

        [Required]
        [MaxLength(40)]
        public string Patronymic { get; set; }

        public DateTime DateAge { get; set; }
        
        [MaxLength(300)]
        public string Address { get; set; }

        public Expression GetExpression()
        {
            Expression<Func<Person, bool>> exp = null;
            
            if (!string.IsNullOrEmpty(Name))
                exp = AddExpression(exp, x => x.Name.StartsWith(Name));
            if (!string.IsNullOrEmpty(Surname))
                exp = AddExpression(exp, x => x.Surname.StartsWith(Surname));
            if (DateAge > DateTime.MinValue)
            {
                var timeMin = TimeSpan.Parse("00:00");
                var timeMax = TimeSpan.Parse("23:59");
                var dateMin = DateAge.Add(timeMin);
                var dateMax = DateAge.Add(timeMax);
                exp = AddExpression(exp, x => x.DateAge >= dateMin && x.DateAge <= dateMax);
            }

            return exp;
        }

        private Expression<Func<Person, bool>> AddExpression(Expression<Func<Person, bool>> exp, Expression<Func<Person, bool>> expAdd)
        {
            if (exp == null)
            {
                exp = expAdd;
            }
            else
            {
                var parameter = Expression.Parameter(typeof(Person));

                var leftVisitor = new ReplaceExpressionVisitor(exp.Parameters[0], parameter);
                var left = leftVisitor.Visit(exp.Body);

                var rightVisitor = new ReplaceExpressionVisitor(expAdd.Parameters[0], parameter);
                var right = rightVisitor.Visit(expAdd.Body);

                exp = Expression.Lambda<Func<Person, bool>>(
                    Expression.AndAlso(left, right), parameter);
            }

            return exp;
        }
    }
}
