﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSocial.Api.Models
{
    public class Relatives : EntityBase
    {
        public int PersonId { get; set; }

        public int PersonRelativeId { get; set; }

        [ForeignKey(nameof(PersonRelativeId))]
        public virtual Person Person { get; set; }
    }
}
