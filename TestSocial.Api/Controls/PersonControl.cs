﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestSocial.Api.Context;
using TestSocial.Api.Infrastructure;
using TestSocial.Api.Models;

namespace TestSocial.Api.Controls
{
    public class PersonControl : IDisposable
    {
        private TestSocialContext _context = new TestSocialContext();
        
        public List<Person> GetAll(Expression<Func<Person, bool>> exp)
        {
            var person = (IQueryable<Person>)_context.Person;
            if (exp != null)
                person = person.Where(exp);

            return person.ToList();
        }

        public Person Get(int id)
        {
            return _context.Person.FirstOrDefault(p => p.Id == id);
        }

        public bool Save(Person person, bool isEdit)
        {
            if (person == null)
                throw new SocialException("Параметр person не должен быть равен null");

            var state = EntityState.Added;
            if (isEdit)
                state = EntityState.Modified;

            _context.Entry(person).State = state;

            return _context.SaveChanges() > 0;
        }

        public bool Delete(int id)
        {
            var person = Get(id);
            if (person == null)
                throw new SocialException("Пользователь с таким Id не найден");

            _context.Entry(person).State = EntityState.Deleted;

            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
