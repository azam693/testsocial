﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestSocial.Api.Context;
using TestSocial.Api.Infrastructure;
using TestSocial.Api.Models;

namespace TestSocial.Api.Controls
{
    public class RelativesControl : IDisposable
    {
        private TestSocialContext _context = new TestSocialContext();

        public List<Relatives> Get(int personId)
        {
            return _context.Relatives.Where(r => r.PersonId == personId).ToList();
        }

        public List<Person> GetPersons(int personId)
        {
            var relatives = Get(personId);
            var persons = new List<Person>();
            foreach (var relative in relatives)
            {
                var person = relative.Person;
                if (person != null)
                {
                    persons.Add(person);
                }
            }

            return persons;
        }

        public bool AddRelative(int personId, int relativeId)
        {
            if (personId == relativeId)
                throw new SocialException("Нельзя добавить связь на себя");

            var relatives = Find(personId, relativeId);
            if (relatives.Count > 0)
                throw new SocialException("Данная связь уже существует");

            var rp = new Relatives { PersonId = personId, PersonRelativeId = relativeId };
            var rr = new Relatives { PersonId = relativeId, PersonRelativeId = personId };

            _context.Relatives.Add(rp);
            _context.Relatives.Add(rr);

            return _context.SaveChanges() > 0;
        }

        public bool Delete(int personId, int relativeId)
        {
            var relatives = Find(personId, relativeId);
            if (relatives.Count == 0)
                throw new SocialException("Не существующая связь");

            foreach (var relative in relatives)
                _context.Relatives.Remove(relative);
            
            return _context.SaveChanges() > 0;
        }

        private List<Relatives> Find(int personId, int relativeId)
        {
            return _context.Relatives.Where(r => (r.PersonId == personId && r.PersonRelativeId == relativeId)
                                                        || (r.PersonId == relativeId && r.PersonRelativeId == personId)).ToList();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
