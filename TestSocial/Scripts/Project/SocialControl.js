﻿var SocialControl = {
    load: function (personId) {
        $.ajax({
            type: "GET",
            url: "/Relatives/Show",
            data: { personId: personId },
            success: function (data) {
                $("#BlockSocial").html(data);
            }
        });
    },

    deleteRecord: function (personId, relativeId) {
        $.ajax({
            type: "GET",
            url: "/Relatives/Delete",
            data: { personId: personId, relativeId: relativeId },
            dataType: "json",
            success: function (data) {
                if (data.IsDelete === true) {
                    SocialControl.load(personId);
                }
            }
        });
    }
}