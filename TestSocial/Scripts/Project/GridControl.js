﻿var GridControl = {
    selectedRow: null,
    keyColumn: "Id",

    openCard: function (url, isEdit) {
        if (isEdit) {
            if (this.selectedRow == null)
                return;

            var id = this.selectedRow[this.keyColumn];
            url = url + id;
        }
        
        document.location.href = url;
    },

    addRecord: function () {
        this.openCard('/Home/Details?Id=0', false);
    },

    editRecord: function () {
        this.openCard('/Home/Details?Id=', true);
    },

    deleteRecord: function () {
        if (this.selectedRow === null)
            return;

        var message = "";
        var id = this.selectedRow[this.keyColumn];
        var deferred = $.Deferred();
        $.ajax({
            type: "GET",
            url: "/Home/DeleteDetails",
            data: { id: id },
            dataType: "json",
            success: function (data) {
                if (data.IsDelete === true) {
                    $("#gridSocial").jsGrid("loadData");
                }
            }
        });
    },

    addRelative: function (personId) {
        if (personId == undefined || personId == null)
            return;

        var id = this.selectedRow[this.keyColumn];
        $.ajax({
            type: "GET",
            url: "/Relatives/AddRelative",
            data: { personId: personId, relativeId: id },
            dataType: "json",
            success: function (data) {
                $("#PanelGridMaster").modal('hide');
                if (data.IsAdd === true) {
                    SocialControl.load(personId);
                }
            }
        });
    }
}
