﻿
//var MyDateField = function (config) {
//    jsGrid.Field.call(this, config);
//};

//MyDateField.prototype = new jsGrid.Field({

//    css: "date-field",            // redefine general property 'css'
//    align: "center",              // redefine general property 'align'

//    myCustomProperty: "foo",      // custom property

//    sorter: function (date1, date2) {
//        return new Date(date1) - new Date(date2);
//    },

//    itemTemplate: function (value) {
//        return new Date(value).toDateString();
//    },

//    insertTemplate: function (value) {
//        return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
//    },

//    editTemplate: function (value) {
//        return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
//    },

//    insertValue: function () {
//        return this._insertPicker.datepicker("getDate").toISOString();
//    },

//    editValue: function () {
//        return this._editPicker.datepicker("getDate").toISOString();
//    }
//});

//jsGrid.fields.date = MyDateField;


var DateField = function (config) {
    jsGrid.Field.call(this, config);
};

DateField.prototype = new jsGrid.Field({
    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },

    itemTemplate: function (value) {
        return $.datepicker.formatDate('dd.mm.yy', new Date(value));
    },

    filterTemplate: function () {
        var now = new Date();
        this._datePicker = $("<input>").datepicker({ defaultDate: now.setFullYear(now.getFullYear() - 1), dateFormat: 'dd.mm.yy' });
        return $("<div>").append(this._datePicker);
    },

    insertTemplate: function (value) {
        return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date(), dateFormat: 'dd.mm.yy' });
    },

    editTemplate: function (value) {
        return this._editPicker = $("<input>").datepicker({ dateFormat: 'dd.mm.yy' }).datepicker("setDate", new Date(value));
    },

    insertValue: function () {
        return this._insertPicker.datepicker("getDate").toISOString();
    },

    editValue: function () {
        return this._editPicker.datepicker("getDate").toISOString();
    },

    filterValue: function () {
        var value = this._datePicker.datepicker("getDate");
        var date = $.datepicker.formatDate('mm.dd.yy', value);
        return date;
    }
});

jsGrid.fields.date = DateField;
