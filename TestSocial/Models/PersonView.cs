﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestSocial.Api.Models;

namespace TestSocial.Models
{
    public class PersonView
    {
        public PersonView()
        {

        }

        public PersonView(Person person)
        {
            Id = person.Id;
            Name = person.Name;
            Surname = person.Surname;
            Patronymic = person.Patronymic;
            DateAge = person.DateAge;
            Address = person.Address;

            CreateStringDate();
        }

        private void CreateStringDate()
        {
            if (DateAge > DateTime.MinValue)
            {
                DateAgeString = DateAge.ToString("d");
            }
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(40)]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [MaxLength(40)]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required]
        [MaxLength(40)]
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }
        
        [Display(Name = "Дата рождения")]
        public DateTime DateAge { get; set; }

        [Required]
        [Display(Name = "Дата рождения")]
        public string DateAgeString { get; set; } = string.Empty;

        [MaxLength(300)]
        [Display(Name = "Адрес")]
        public string Address { get; set; }

        public string ErrorMessage { get; set; }

        public Person CreatePerson()
        {
            var person = new Person
            {
                Id = Id,
                Name = Name,
                Surname = Surname,
                Patronymic = Patronymic,
                Address = Address,
                DateAge = DateAge
            };

            if (!string.IsNullOrEmpty(DateAgeString))
            {
                DateTime dateAge;
                if (DateTime.TryParse(DateAgeString, out dateAge))
                {
                    person.DateAge = dateAge;
                }
            }

            return person;
        }
    }
}