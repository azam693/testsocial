﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using TestSocial.Api.Controls;
using TestSocial.Api.Infrastructure;
using TestSocial.Api.Models;
using TestSocial.Models;

namespace TestSocial.Controllers
{
    public class HomeController : Controller
    {
        private const string ErrorMessage = "Ошибка во время сохранения";

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public string Persons(PersonView view)
        {
            var person = view.CreatePerson();

            return GetPersonsJson(person);
        }
        
        public ActionResult Details(int id)
        {
            ViewBag.IdRecord = id;

            return View();
        }

        [HttpGet]
        public ActionResult AddDetails()
        {
            var view = new PersonView();

            return PartialView(view);
        }

        [HttpGet]
        public ActionResult EditDetails(int id)
        {
            PersonView view = null;
            using (var control = new PersonControl())
            {
                var person = control.Get(id);
                if (person != null)
                    view = new PersonView(person);
            }

            return PartialView(view);
        }

        [HttpPost]
        public ActionResult AddDetails( PersonView view)
        {
            if (!ModelState.IsValid)
                return PartialView(view);

            bool isSave = Save(ref view, 0);
            if (!isSave)
            {
                TempData["ErrorMessage"] = ErrorMessage;
            }

            return RedirectToAction("Details", new { id = view.Id });
        }

        [HttpPost]
        public ActionResult EditDetails(int id, PersonView view)
        {
            if (!ModelState.IsValid)
                return PartialView(view);
            
            bool isSave = Save(ref view, id);
            if (!isSave)
            {
                view.ErrorMessage = ErrorMessage;
            }

            return PartialView(view);
        }

        public JsonResult DeleteDetails(int id)
        {
            string errorMessage = string.Empty;
            bool isDelete = false;
            try
            {
                using (var control = new PersonControl())
                {
                    isDelete = control.Delete(id);
                }
            }
            catch (SocialException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                errorMessage = "Ошибка во время удаления записи";
            }

            return Json(new { IsDelete = isDelete, Error = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        private List<PersonView> CreatePersonView(List<Person> persons)
        {
            var views = new List<PersonView>();
            foreach (var person in persons)
            {
                var view = new PersonView(person);
                views.Add(view);
            }

            return views;
        }

        private string GetPersonsJson(Person person)
        {
            var persons = new List<Person>();
            using (var control = new PersonControl())
            {
                var exp = (Expression<Func<Person, bool>>)person.GetExpression();
                persons = control.GetAll(exp);
            }

            var views = CreatePersonView(persons);
            return JsonConvert.SerializeObject(views);
        }

        private bool Save(ref PersonView view, int id)
        {
            view.Id = id;
            var person = view.CreatePerson();
            bool isSave = false;
            bool isEdit = id > 0;
            try
            {
                using (var control = new PersonControl())
                {
                    isSave = control.Save(person, isEdit);
                    view.Id = person.Id;
                }
            }
            catch (Exception ex)
            {
                
            }

            return isSave;
        }
    }
}