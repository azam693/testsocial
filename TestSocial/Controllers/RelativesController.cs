﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSocial.Api.Controls;
using TestSocial.Api.Infrastructure;
using TestSocial.Api.Models;
using TestSocial.Models;

namespace TestSocial.Controllers
{
    public class RelativesController : Controller
    {
        // GET: Relatives
        public ActionResult Show(int personId)
        {
            var views = new List<PersonView>();
            var persons = new List<Person>();
            using (var control = new RelativesControl())
            {
                persons = control.GetPersons(personId);
            }

            foreach (var person in persons)
            {
                var view = new PersonView(person);
                views.Add(view);
            }

            ViewBag.PersonId = personId;

            return PartialView(views);
        }

        public ActionResult Delete(int personId, int relativeId)
        {
            bool isDelete = false;
            string errorMessage = string.Empty;
            try
            {
                using (var control = new RelativesControl())
                {
                    isDelete = control.Delete(personId, relativeId);
                }
            }
            catch (SocialException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                errorMessage = "Ошибка во время удаления записи";
            }

            return Json(new { IsDelete = isDelete, ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRelative(int personId, int relativeId)
        {
            bool isAdd = false;
            string errorMessage = string.Empty;
            try
            {
                using (var control = new RelativesControl())
                {
                    isAdd = control.AddRelative(personId, relativeId);
                }
            }
            catch (SocialException ex)
            {
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                errorMessage = "Ошибка во время добавления связи";
            }

            return Json(new { IsAdd = isAdd, ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GridMaster(int personId)
        {
            return PartialView(personId);
        }
    }
}